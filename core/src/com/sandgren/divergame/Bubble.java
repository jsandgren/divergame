package com.sandgren.divergame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.TimeUtils;

public class Bubble implements Sprite {

	private float x;
	private float y;
	private float y_moved;
	private static Texture[] texture = null;
	private int currentIndex = 0;
	private int currentSprite = 0;
	private long spriteUpdateFrequency;
	private long spriteTimer;

	public Bubble(int x, int y) {
		this.x = (float)x;
		this.y = (float)y;	
		y_moved = 0;
		
		if (texture == null)
		{
			texture = new Texture[2];
			texture[0] = new Texture(Gdx.files.internal("sprite_bubble1.png"));
			texture[1] = new Texture(Gdx.files.internal("sprite_bubble2.png"));
		}
		
		currentIndex = 0;
		currentSprite = 0;
		spriteUpdateFrequency = 500;

		spriteTimer = TimeUtils.millis();
	}

	@Override
	public float getX() {
		return x;
	}

	@Override
	public float getY() {
		return y;
	}

	@Override
	public Texture getTexture() {
		if (TimeUtils.timeSinceMillis(spriteTimer) > spriteUpdateFrequency)
		{
			spriteTimer = TimeUtils.millis();
			currentSprite++;
			
			if (currentSprite > 1)
			{
				currentSprite = 0;
			}
		}
				
		return texture[currentIndex + currentSprite];
	}

	@Override
	public void move(int x, int y) {
		y_moved = y_moved + 0.5f;
		this.x = (float) (this.x + (-0.1f + (Math.random() * 0.2f)));
		this.y = this.y + 0.5f;
	}
	
	public boolean isDead()
	{
//		return y_moved > 300f;
		return y > 480;
	}

	@Override
	public void hit(int force) {
		// TODO Auto-generated method stub		
	}

	@Override
	public float getScaling() {
		// TODO Auto-generated method stub
		return 0.5f * (y_moved / 300f);
	}

	@Override
	public float getRotation() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean getFlipX() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getFlipY() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public float getWidth() {
		// TODO Auto-generated method stub
		return getTexture().getWidth();
	}

	@Override
	public float getHeight() {
		return getTexture().getHeight();
	}

	@Override
	public void draw(SpriteBatch batch) {
		batch.draw(this.getTexture(), this.getX(), this.getY(), 1f, 1f, 
				this.getWidth(), this.getHeight(), 
  		this.getScaling(), this.getScaling(), this.getRotation(), 0, 0, (int)this.getWidth(), (int)this.getHeight(), this.getFlipX(), this.getFlipY());
		
	}

	@Override
	public ArrayList<Rectangle> getLifeRects() {
		// TODO Auto-generated method stub
		return null;
	}

}
