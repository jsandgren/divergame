package com.sandgren.divergame;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class CoralReef implements Sprite {
	private ArrayList<SeaWeed> seaweeds = new ArrayList<SeaWeed>();
	private ArrayList<Stone> stones = new ArrayList<Stone>();

	public CoralReef()
	{
		for(int i = 0; i < 50; i++)
		{
			seaweeds.add(new SeaWeed((int) (Math.random()*1280), 0, (Math.random() > 0.5)));
		}
		
		for(int i = 0; i < 30; i++)
		{
			stones.add(new Stone((int) (Math.random()*1280), 0));
		}
	}
	
	@Override
	public float getX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Texture getTexture() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void move(int x, int y) {
		// TODO Auto-generated method stub

	}

	@Override
	public void hit(int force) {
		// TODO Auto-generated method stub

	}

	@Override
	public float getScaling() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getRotation() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean getFlipX() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getFlipY() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public float getWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void draw(SpriteBatch batch) {
		for (SeaWeed sw : seaweeds)
		{
			if (sw != null){
				sw.draw(batch);				
			}			
		}		
		for (Stone s : stones)
		{
			if (s != null){
				s.draw(batch);				
			}			
		}		

	}

	@Override
	public ArrayList<Rectangle> getLifeRects() {
		// TODO Auto-generated method stub
		return null;
	}

}
