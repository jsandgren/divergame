package com.sandgren.divergame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.TimeUtils;

public class Diver implements Sprite {
	
	private Texture[] texture = new Texture[5];
	private int currentIndex = 0;
	private int currentSprite = 0;
	private int x;
	private int y;
	private int state;
	private int hp;
	private long spriteTimer;
	private long lastMovement;
	private long spriteUpdateFrequency;
	private boolean flipX = false;
	private ArrayList<Bubble> bubbles = new ArrayList<Bubble>();
	private long lastBubbles;
	private long lastHit;
	
	public Diver(int hp, int x, int y)
	{
		this.x = x;
		this.y = y;
		this.hp = hp; 

		texture[0] = new Texture(Gdx.files.internal("sprite_diver1.png"));
		texture[1] = new Texture(Gdx.files.internal("sprite_diver2.png"));
		texture[2] = new Texture(Gdx.files.internal("sprite_diver3.png"));
		texture[3] = new Texture(Gdx.files.internal("sprite_diver4.png"));
		currentIndex = 0;
		currentSprite = 0;
		spriteUpdateFrequency = 500;

		spriteTimer = TimeUtils.millis();
		lastBubbles = TimeUtils.millis();
		lastHit = TimeUtils.millis();
	}
	
	public Texture getTexture()
	{
		if (TimeUtils.timeSinceMillis(lastMovement) > 500)
		{
			spriteUpdateFrequency = 500;	
			currentIndex = 0;
		}
		
		if (TimeUtils.timeSinceMillis(spriteTimer) > spriteUpdateFrequency)
		{
			spriteTimer = TimeUtils.millis();
			currentSprite++;
			
			if (currentSprite > 1)
			{
				currentSprite = 0;
			}
		}
		
		handleBubbles();
		
		return texture[currentIndex + currentSprite];
	}
	
	private void handleBubbles() {
	
		if (TimeUtils.timeSinceMillis(lastBubbles) > 100)
		{
			lastBubbles = TimeUtils.millis();
			bubbles.add(new Bubble((int) ((x - 5) + (Math.random() * 10)) , y + 35));
		}

		if (TimeUtils.timeSinceMillis(lastBubbles) > 10)
		{
			ArrayList<Bubble> tmp = new ArrayList<Bubble>();
			for (Bubble b : bubbles)
			{
				if (b != null){
					b.move(0, 0);
					
					if (b.isDead())
					{
						tmp.add(b);
					}
				}			
			}		

			for (Bubble b : tmp)
			{
				bubbles.remove(b);
			}
			
			tmp.clear();
		}		
	}

	public float getX()
	{
		return x;
	}
	
	public float getY()
	{
		return y;
	}
	
	public int getHP()
	{
		return hp;
	}
	
	public void move(int x, int y)
	{
		this.x = this.x + x;
		this.y = this.y + y;
		lastMovement = TimeUtils.millis();
		spriteUpdateFrequency = 100;
		
		if (x != 0 && y == 0)
		{
			currentIndex = 2;	
			
			if (x < 0)
			{
				this.flipX = true;
			}
			else
			{
				this.flipX = false;				
			}		
		}
		else if (y != 0)
		{
			currentIndex = 0;			
		}
		

		
		//System.out.println("diver move(" + x +", " + y + ") index: " + currentIndex);
	}
	
	public void hit(int force)
	{
		this.hp = this.hp - force;	
		lastHit = TimeUtils.millis();
	}

	public void dispose() {
		for (Texture texture : this.texture)
		{
			if (texture != null){
				texture.dispose();				
			}			
		}		
	}

	@Override
	public float getScaling() {
		float scaling = 1;
		
		if (TimeUtils.timeSinceMillis(lastHit) < 500)
		{
			//scaling = (500 + (TimeUtils.timeSinceMillis(lastHit))) / 1000;
			scaling = 0.5f;
			//System.out.println("diver scaling " + scaling);
		}
		
		return scaling;
	}

	@Override
	public float getRotation() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean getFlipX() {
		// TODO Auto-generated method stub
		return flipX;
	}

	@Override
	public boolean getFlipY() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public float getWidth() {
		return texture[currentIndex + currentSprite].getWidth();
	}

	@Override
	public float getHeight() {
		return texture[currentIndex + currentSprite].getHeight();
	}

	public void draw(SpriteBatch batch) {
		batch.draw(this.getTexture(), this.getX(), this.getY(), 1f, 1f, 
				this.getWidth(), this.getHeight(), 
  		this.getScaling(), this.getScaling(), this.getRotation(), 0, 0, (int)this.getWidth(), (int)this.getHeight(), this.getFlipX(), this.getFlipY());

		for (Bubble b : bubbles)
		{
			if (b != null){
				b.draw(batch);				
			}			
		}		
    }

	@Override
	public ArrayList<Rectangle> getLifeRects(){
		Rectangle lifeRect = new Rectangle(this.x, this.y, texture[0].getWidth(), texture[0].getHeight());
		ArrayList<Rectangle> tmpList = new ArrayList<Rectangle>();
		tmpList.add(lifeRect);
		return tmpList;
	}
}
