package com.sandgren.divergame;

import java.util.ArrayList;

import java.util.Iterator;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;

public class GameHandler implements ApplicationListener, ControllerListener {
   private Sound dropSound;
   private Music rainMusic;
   private SpriteBatch batch;
   private OrthographicCamera camera;
   private Diver diver;
   private Shark shark;
   private CoralReef coralreef;
   private BitmapFont font;
   private int score = 0;
   private Texture fontTex;
   private TextureRegion rect;
   private float oxygenlevel = 100;
private boolean inMotion;
private float xMove;
private float yMove;

   @Override
   public void create() {
	   // load the images for the droplet and the bucket, 64x64 pixels each
	   diver = new Diver(10, 50, 50);
	   shark = new Shark(10, 400, 400);
	   coralreef = new CoralReef();  

	   font = new BitmapFont();
	   font.setColor(Color.DARK_GRAY);
	   font.setScale(1f);

	   // load the drop sound effect and the rain background "music"
	   dropSound = Gdx.audio.newSound(Gdx.files.internal("drop.wav"));
	   rainMusic = Gdx.audio.newMusic(Gdx.files.internal("rain.mp3"));

	   // start the playback of the background music immediately
	   rainMusic.setLooping(true);
	   rainMusic.play();

	   // create the camera and the SpriteBatch
	   camera = new OrthographicCamera();
	   camera.setToOrtho(false, 800, 480);
	   batch = new SpriteBatch();
      
	   fontTex = new Texture("sprite_oxygenbar1.png");

	   //in Photoshop, we included a small white box at the bottom right of our font sheet
	   //we will use this to draw lines and rectangles within the same batch as our text
	   rect = new TextureRegion(fontTex, fontTex.getWidth()-2, fontTex.getHeight()-2, 1, 1);
	   
	   Controllers.addListener(this);
	   
	   for (Controller controller : Controllers.getControllers()) {
		   System.out.println("controller: " + controller.getName());
		}
	   
   }

   @Override
   public void render() {
      // clear the screen with a dark blue color. The
      // arguments to glClearColor are the red, green
      // blue and alpha component in the range [0,1]
      // of the color to be used to clear the screen.
      Gdx.gl.glClearColor(0.199f, 0.402f, 0.594f, 1);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      // tell the camera to update its matrices.
      camera.update();

      // tell the SpriteBatch to render in the
      // coordinate system specified by the camera.
      batch.setProjectionMatrix(camera.combined);
      
      batch.begin();
      diver.draw(batch);
      shark.draw(batch);
      coralreef.draw(batch);
      batch.end();
      
      // process user input
      if(Gdx.input.isTouched()) {
         Vector3 touchPos = new Vector3();
         touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
         camera.unproject(touchPos);
      }
      if(Gdx.input.isKeyPressed(Keys.LEFT)) diver.move((int)(-200 * Gdx.graphics.getDeltaTime()), 0);
      if(Gdx.input.isKeyPressed(Keys.RIGHT)) diver.move((int)(200 * Gdx.graphics.getDeltaTime()), 0);
      if(Gdx.input.isKeyPressed(Keys.DOWN)) diver.move(0, (int)(-200 * Gdx.graphics.getDeltaTime()));
      if(Gdx.input.isKeyPressed(Keys.UP)) diver.move(0, (int)(200 * Gdx.graphics.getDeltaTime()));

      if (this.inMotion)
      {
    	  diver.move((int)(200 * Gdx.graphics.getDeltaTime() * xMove), (int)(200 * Gdx.graphics.getDeltaTime() * yMove));  	  
      }
      
      shark.move(0, 0);
      checkCollisions(diver,shark);
      
   }

   private void checkCollisions(Diver diver, Shark shark) {
	   ArrayList<Rectangle> diverList = diver.getLifeRects();
	   ArrayList<Rectangle> sharkList = shark.getLifeRects();
	   
	   Rectangle diverRect = diverList.get(0);
	   
		for (Rectangle r : sharkList)
		{
			if (r != null){		
				Rectangle intersection = new Rectangle();
				if (Intersector.intersectRectangles(diverRect, r, intersection))
				{
					diver.hit(0);
					//System.out.println("checkCollisions Diver hit");
				}
			}			
		}		
	
}

@Override
	public void dispose() {
		// dispose of all the native resources
	
		diver.dispose();
		dropSound.dispose();
		rainMusic.dispose();
		batch.dispose();
	}

   @Override
   public void resize(int width, int height) {
   }

   @Override
   public void pause() {
   }

   @Override
   public void resume() {
   }

@Override
public void connected(Controller controller) {
	// TODO Auto-generated method stub
	   System.out.println("controller connected");
	
}

@Override
public void disconnected(Controller controller) {
	// TODO Auto-generated method stub
	   System.out.println("controller disconnected");
	
}

@Override
public boolean buttonDown(Controller controller, int buttonCode) {
	   System.out.println("controller buttonDown " + buttonCode);
	return false;
}

@Override
public boolean buttonUp(Controller controller, int buttonCode) {
	   System.out.println("controller buttonUp " + buttonCode);
	return false;
}

@Override
public boolean axisMoved(Controller controller, int axisCode, float value) {
	   System.out.println("controller axisMoved " + axisCode + " " + value);

	   if (axisCode == 0) // Y
	   {
		   this.yMove = -value;
		   
	   } else if (axisCode == 1) // X
	   {
		   this.xMove = value;	   
	   }

	   if (Math.abs(yMove) < 0.1){
		   yMove = 0.00f;
	   }
	   if (Math.abs(xMove) < 0.1){
		   xMove = 0.00f;
	   }
	   
	   inMotion = ((yMove != 0) || (xMove != 0));
	   
	return false;
}

@Override
public boolean povMoved(Controller controller, int povCode, PovDirection value) {
	   System.out.println("controller povMoved " + povCode + " " + value);

	   switch(value)
	   {
		case center:
			this.inMotion = false;
			break;
		case east:
			this.inMotion = true;
			this.xMove = 1;
			this.yMove = 0;
			break;
		case north:
			this.inMotion = true;
			this.xMove = 0;
			this.yMove = 1;
			break;
		case northEast:
			this.inMotion = true;
			this.xMove = 1;
			this.yMove = 1;
			break;
		case northWest:
			this.inMotion = true;
			this.xMove = -1;
			this.yMove = 1;
			break;
		case south:
			this.inMotion = true;
			this.xMove = 0;
			this.yMove = -1;
			break;
		case southEast:
			this.inMotion = true;
			this.xMove = 1;
			this.yMove = -1;
			break;
		case southWest:
			this.inMotion = true;
			this.xMove = -1;
			this.yMove = -1;
			break;
		case west:
			this.inMotion = true;
			this.xMove = -1;
			this.yMove = 0;
			break;
		default:
			this.inMotion = false;
			this.xMove = 0;
			this.yMove = 0;
			break;
	   }

	      if(value == PovDirection.west) diver.move((int)(-200 * Gdx.graphics.getDeltaTime()), 0);
	      if(value == PovDirection.east) diver.move((int)(200 * Gdx.graphics.getDeltaTime()), 0);
	      if(value == PovDirection.south) diver.move(0, (int)(-200 * Gdx.graphics.getDeltaTime()));
	      if(value == PovDirection.north) diver.move(0, (int)(200 * Gdx.graphics.getDeltaTime()));
	return false;
}

@Override
public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
	   System.out.println("controller xSliderMoved " + sliderCode + " " + value);
	// TODO Auto-generated method stub
	return false;
}

@Override
public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
	   System.out.println("controller ySliderMoved " + sliderCode + " " + value);
	// TODO Auto-generated method stub
	return false;
}

@Override
public boolean accelerometerMoved(Controller controller, int accelerometerCode,
		Vector3 value) {
	   System.out.println("controller accelerometerMoved " + accelerometerCode + " " + value);
	// TODO Auto-generated method stub
	return false;
}
}