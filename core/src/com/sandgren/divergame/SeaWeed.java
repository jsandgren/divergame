package com.sandgren.divergame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.TimeUtils;

public class SeaWeed implements Sprite {

	private float x;
	private float y;
	private float scaling;
	private Texture[] texture = null;
	private int currentIndex = 0;
	private int currentSprite = 0;
	private boolean spriteReversed = false;
	private long spriteUpdateFrequency;
	private long spriteTimer;
	private boolean flipX;

	public SeaWeed(int x, int y, boolean flipX)
	{
		this.x = (float)x;
		this.y = (float)y;	

		if (texture == null)
		{
			texture = new Texture[4];
			texture[0] = new Texture(Gdx.files.internal("sprite_seaweed1.png"));
			texture[1] = new Texture(Gdx.files.internal("sprite_seaweed2.png"));
			texture[2] = new Texture(Gdx.files.internal("sprite_seaweed3.png"));
			texture[3] = new Texture(Gdx.files.internal("sprite_seaweed4.png"));
		}
		
		currentIndex = 0;
		spriteUpdateFrequency = 300;
		this.flipX = flipX;
		
		if (flipX)
		{
			currentSprite = 3;	
			spriteReversed = true;
		}
		else
		{
			currentSprite = 0;			
		}

		spriteTimer = TimeUtils.millis();
		this.scaling = (float) (0.5f + (Math.random()));
	}

	@Override
	public float getX() {
		return x;
	}

	@Override
	public float getY() {
		return y;
	}

	@Override
	public Texture getTexture() {
		if (TimeUtils.timeSinceMillis(spriteTimer) > spriteUpdateFrequency)
		{
			spriteTimer = TimeUtils.millis();
			
			if (spriteReversed)
			{
				currentSprite--;				
			}
			else
			{
				currentSprite++;
			}
			
			if (currentSprite > 3)
			{
				currentSprite = 2;
				spriteReversed = true;
			}
			else if (currentSprite < 0)
			{
				currentSprite = 1;
				spriteReversed = false;
			}
		}
				
		return texture[currentIndex + currentSprite];
	}

	@Override
	public void move(int x, int y) {

	}
	
	public boolean isDead()
	{
		return false;
	}

	@Override
	public void hit(int force) {
		// TODO Auto-generated method stub		
	}

	@Override
	public float getScaling() {
		return scaling;
	}

	@Override
	public float getRotation() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean getFlipX() {
		// TODO Auto-generated method stub
		return flipX;
	}

	@Override
	public boolean getFlipY() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public float getWidth() {
		// TODO Auto-generated method stub
		return getTexture().getWidth();
	}

	@Override
	public float getHeight() {
		return getTexture().getHeight();
	}

	@Override
	public void draw(SpriteBatch batch) {
		batch.draw(this.getTexture(), this.getX(), this.getY(), 1f, 1f, 
				this.getWidth(), this.getHeight(), 
  		this.getScaling(), this.getScaling(), this.getRotation(), 0, 0, (int)this.getWidth(), (int)this.getHeight(), this.getFlipX(), this.getFlipY());
		
	}

	@Override
	public ArrayList<Rectangle> getLifeRects() {
		// TODO Auto-generated method stub
		return null;
	}

}
