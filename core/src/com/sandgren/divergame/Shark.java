package com.sandgren.divergame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.TimeUtils;

public class Shark implements Sprite {

	private enum SharkState {INIT, NORMAL, SPITTING};
	private Texture[] texture = new Texture[5];
	private int currentIndex = 0;
	private int currentSprite = 0;
	private int x;
	private int y;
	private SharkState state;
	private int hp;
	private long spriteTimer;
	private long lastMovement;
	private long spriteUpdateFrequency;
	private long createdTimer;
	private int spits = 10;
	private boolean flipX = false;
	private long lastWhirls;
	private ArrayList<Whirl> whirls = new ArrayList<Whirl>();
	
	public Shark(int hp, int x, int y)
	{
		this.x = x;
		this.y = y;
		this.hp = hp;

		texture[0] = new Texture(Gdx.files.internal("sprite_shark1.png"));
		texture[1] = new Texture(Gdx.files.internal("sprite_shark2.png"));
		texture[2] = new Texture(Gdx.files.internal("sprite_shark3.png"));
		texture[3] = new Texture(Gdx.files.internal("sprite_shark4.png"));
		texture[4] = new Texture(Gdx.files.internal("sprite_shark5.png"));
		currentIndex = 0;
		currentSprite = 0;
		spriteUpdateFrequency = 500;
		state = SharkState.NORMAL;
		createdTimer = TimeUtils.millis();
		spriteTimer = TimeUtils.millis();
		lastWhirls = TimeUtils.millis();
	}
	
	
	@Override
	public float getX() {
		// TODO Auto-generated method stub
		return x;
	}

	@Override
	public float getY() {
		// TODO Auto-generated method stub
		return y;
	}

	public void hit(int force)
	{
		this.hp = this.hp - force;
	}
	@Override
	public Texture getTexture()
	{
		int maxSprite = 0;
		
		//if (TimeUtils.timeSinceMillis(lastMovement) > 500)
		{
			if (state == SharkState.NORMAL)
			{
				currentIndex = 0;
				maxSprite = 1;
				spriteUpdateFrequency = 500;
			}
			else if (state == SharkState.SPITTING)
			{
				currentIndex = 2;
				maxSprite = 2;
				spriteUpdateFrequency = 200;
			}
		}
		
		if (TimeUtils.timeSinceMillis(spriteTimer) > spriteUpdateFrequency)
		{
			spriteTimer = TimeUtils.millis();
			currentSprite++;
			
			if (currentSprite > maxSprite)
			{
				//System.out.println("shark getTexture " + state.toString() + " " + currentSprite + " " + maxSprite);	
				currentSprite = 0;

				if (state == SharkState.SPITTING)
				{
					state = SharkState.NORMAL;	
					whirls.add(new Whirl((this.x - 20), this.y + 20));	
					//System.out.println("Shark new whirl");
				}
			}
		}
		
		return texture[currentIndex + currentSprite];
	}

	private void handleWhirls() {
//		if (TimeUtils.timeSinceMillis(lastWhirl) > 3000)
//		{
//			lastBubbles = TimeUtils.millis();
//			bubbles.add(new Bubble((int) ((x - 5) + (Math.random() * 10)) , y + 35));
//		}

		if (TimeUtils.timeSinceMillis(lastWhirls) > 100)
		{
			lastWhirls = TimeUtils.millis();
			ArrayList<Whirl> tmp = new ArrayList<Whirl>();
			for (Whirl w : whirls )
			{
				if (w != null){
					w.move(0, 0);
					
					if (w.isDead())
					{
						tmp.add(w);
					}
				}			
			}		

			for (Whirl w : tmp)
			{
				whirls.remove(w);
			}
			
			tmp.clear();
		}	
		
	}


	@Override
	public void move(int x, int y) {
		int moveX = 0;

		if (state == SharkState.NORMAL)
		{			
			if (!flipX)
			{
				moveX = -1;
			}
			else
			{
				moveX = 1;
			}
			
			if (TimeUtils.timeSinceMillis(lastMovement) > 20)
			{
				lastMovement = TimeUtils.millis();
				spriteUpdateFrequency = 250;
				
	
				this.x = this.x + moveX;
				this.y = this.y + 0;

				if (this.x < 20)
				{
					flipX = true;
					//System.out.println("shark move flip: " + flipX);
					
				}
				else if (this.x > 1200)
				{
					flipX = false;
					//System.out.println("shark move flip: " + flipX);
				}
			}
			
			if (spits > 0 && TimeUtils.timeSinceMillis(createdTimer) > 3000)
			{
				//System.out.println("shark Spitting move(" + x +", " + y + ") index: " + currentIndex);
				state = SharkState.SPITTING;
				spits--;
	
				spriteUpdateFrequency = 500;
				currentSprite = 0;
				createdTimer = TimeUtils.millis();
			}	
		}
		

		handleWhirls();
	}


	@Override
	public float getScaling() {
		// TODO Auto-generated method stub
		return 3;
	}


	@Override
	public float getRotation() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public boolean getFlipX() {
		return flipX;
	}


	@Override
	public boolean getFlipY() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public float getWidth() {
		return texture[currentIndex + currentSprite].getWidth();
	}


	@Override
	public float getHeight() {
		return texture[currentIndex + currentSprite].getHeight();
	}

	@Override
	public void draw(SpriteBatch batch) {
		batch.draw(this.getTexture(), this.getX(), this.getY(), 1f, 1f, 
				this.getWidth(), this.getHeight(), 
  		this.getScaling(), this.getScaling(), this.getRotation(), 0, 0, (int)this.getWidth(), (int)this.getHeight(), this.getFlipX(), this.getFlipY());

		for (Whirl w : whirls)
		{
			if (w != null){
				w.draw(batch);				
			}			
		}		
    }

	public ArrayList<Rectangle> getLifeRects(){
		ArrayList<Rectangle> tmpList = new ArrayList<Rectangle>();
		Rectangle lifeRect = new Rectangle(this.x, this.y, texture[0].getWidth(), texture[0].getHeight());
		
		tmpList.add(lifeRect);

		for (Whirl w : whirls)
		{
			if (w != null){
				tmpList.addAll(w.getLifeRects());			
			}			
		}		
		
		return tmpList;
	}
		
}