package com.sandgren.divergame;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public interface Sprite {

	public float getX();
	public float getY();
	public Texture getTexture();
	public void move(int x, int y);
	public void hit(int force);
	public float getScaling();
	public float getRotation();
	public boolean getFlipX();
	public boolean getFlipY();
	public float getWidth();
	public float getHeight();
	public ArrayList<Rectangle> getLifeRects();

	public void draw(SpriteBatch batch);
}
