package com.sandgren.divergame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Stone implements Sprite {

	private float x;
	private float y;
	private float scaling;
	private Texture[] texture = null;
	private int currentIndex = 0;
	private boolean flipX;
	
	public Stone(int x, int y) 
	{
		this.x = x;
		this.y = y;		
		
		if (texture == null)
		{
			texture = new Texture[6];
			texture[0] = new Texture(Gdx.files.internal("sprite_stone1.png"));
			texture[1] = new Texture(Gdx.files.internal("sprite_stone2.png"));
			texture[2] = new Texture(Gdx.files.internal("sprite_stone3.png"));
			texture[3] = new Texture(Gdx.files.internal("sprite_stone4.png"));
			texture[4] = new Texture(Gdx.files.internal("sprite_stone5.png"));
			texture[5] = new Texture(Gdx.files.internal("sprite_stone6.png"));
		}
		
		currentIndex = (int) (Math.random() * 6);
		
		if (Math.random() > 0.5)
		{
			flipX = true;			
		}
		else
		{
			flipX = false;
		}		

		scaling = (float) (0.5f + (Math.random()));
	}

	@Override
	public float getX() {
		return x;
	}

	@Override
	public float getY() {
		return y;
	}

	@Override
	public Texture getTexture() {
				
		return texture[currentIndex];
	}

	@Override
	public void move(int x, int y) {

	}
	
	public boolean isDead()
	{
		return false;
	}

	@Override
	public void hit(int force) {
		// TODO Auto-generated method stub		
	}

	@Override
	public float getScaling() {
		return scaling;
	}

	@Override
	public float getRotation() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean getFlipX() {
		// TODO Auto-generated method stub
		return flipX;
	}

	@Override
	public boolean getFlipY() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public float getWidth() {
		// TODO Auto-generated method stub
		return getTexture().getWidth();
	}

	@Override
	public float getHeight() {
		return getTexture().getHeight();
	}

	@Override
	public void draw(SpriteBatch batch) {
		batch.draw(this.getTexture(), this.getX(), this.getY(), 1f, 1f, 
				this.getWidth(), this.getHeight(), 
  		this.getScaling(), this.getScaling(), this.getRotation(), 0, 0, (int)this.getWidth(), (int)this.getHeight(), this.getFlipX(), this.getFlipY());
		
	}

	@Override
	public ArrayList<Rectangle> getLifeRects() {
		// TODO Auto-generated method stub
		return null;
	}
}
