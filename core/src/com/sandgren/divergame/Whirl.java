package com.sandgren.divergame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.TimeUtils;

public class Whirl implements Sprite {

	private float x;
	private float y;
	private Texture[] texture = new Texture[3];
	private int currentIndex = 0;
	private int currentSprite = 0;
	private long spriteUpdateFrequency;
	private long spriteTimer;
	
	public Whirl(int x, int y)
	{
		this.x = (float)x;
		this.y = (float)y;	
		texture[0] = new Texture(Gdx.files.internal("sprite_virvel1.png"));
		texture[1] = new Texture(Gdx.files.internal("sprite_virvel2.png"));
		texture[2] = new Texture(Gdx.files.internal("sprite_virvel3.png"));
		currentIndex = 0;
		currentSprite = 0;
		spriteUpdateFrequency = 500;

		spriteTimer = TimeUtils.millis();
	}

	@Override
	public float getX() {
		return x;
	}

	@Override
	public float getY() {
		return y;
	}

	@Override
	public Texture getTexture() {
		if (TimeUtils.timeSinceMillis(spriteTimer) > spriteUpdateFrequency)
		{
			spriteTimer = TimeUtils.millis();
			currentSprite++;
			
			if (currentSprite > 2)
			{
				currentSprite = 0;
			}
		}
				
		return texture[currentIndex + currentSprite];
	}

	@Override
	public void move(int x, int y) {
		this.y = (float) (this.y + (-0.1f + (Math.random() * 0.2f)));
		this.x = this.x - 3f;
	}
	
	public boolean isDead()
	{
		boolean isdead = x < 0f;
		return isdead;
	}

	@Override
	public void hit(int force) {	
	}

	@Override
	public float getScaling() {
//		return 0.5f * (y_moved / 300f);
		return 1f;
	}

	@Override
	public float getRotation() {
		return 0;
	}

	@Override
	public boolean getFlipX() {
		return false;
	}

	@Override
	public boolean getFlipY() {
		return false;
	}

	@Override
	public float getWidth() {
		return getTexture().getWidth();
	}

	@Override
	public float getHeight() {
		return getTexture().getHeight();
	}

	@Override
	public void draw(SpriteBatch batch) {
		batch.draw(this.getTexture(), this.getX(), this.getY(), 1f, 1f, 
				this.getWidth(), this.getHeight(), 
  		this.getScaling(), this.getScaling(), this.getRotation(), 0, 0, (int)this.getWidth(), (int)this.getHeight(), this.getFlipX(), this.getFlipY());
		
	}

	public ArrayList<Rectangle> getLifeRects(){
		Rectangle lifeRect = new Rectangle(this.x, this.y, texture[0].getWidth(), texture[0].getHeight());
		ArrayList<Rectangle> tmpList = new ArrayList<Rectangle>();
		tmpList.add(lifeRect);
		return tmpList;
	}

}
