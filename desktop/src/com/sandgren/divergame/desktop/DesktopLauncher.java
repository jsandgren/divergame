package com.sandgren.divergame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.sandgren.divergame.GameHandler;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
      config.title = "Diver";
      config.width = 1280;
      config.height = 720;
      //config.fullscreen = true;
      new LwjglApplication(new GameHandler(), config);
	}
}
